#!/bin/bash

source ./box.sh
source ./variables.sh

box "Starting Docker Machine creation" "green" "blue"
echo $leaders
for node in $(seq 1 $leaders);
do
   box "Node leader $node" "light_purple" "red"
   docker-machine create \
      --engine-env 'DOCKER_OPTS="-H unix:///var/run/docker.sock"' \
      --driver virtualbox \
   leader$node
done

for node in $(seq 1 $workers);
do
   box "Node worker $node" "light_purple" "red"
   docker-machine create \
      --engine-env 'DOCKER_OPTS="-H unix:///var/run/docker.sock"' \
      --engine-label instance=worker${node} \
      --driver virtualbox \
   worker${node}
done

eval "$(docker-machine env leader1)"
box "Init Swarm cluster" "light_purple" "blue"
docker swarm init --listen-addr $(docker-machine ip leader1) --advertise-addr $(docker-machine ip leader1)
token=$(docker swarm join-token -q worker)

for node in $(seq 1 $workers);
do
   eval "$(docker-machine env worker$node)"
   docker swarm join --token $token $(docker-machine ip leader1):2377
done

eval $(docker-machine env leader1)
box "Overlay Network creation" "light_purple" "blue"
docker network create -d overlay swarmnet
box "Starting WordPress stack" "light_green" "green"
eval $(docker-machine env leader1) && docker stack deploy --compose-file docker-compose.yml stack
sleep $t


docker run -it -d -p 8080:8080 -e HOST=$(docker-machine ip leader1)  -v /var/run/docker.sock:/var/run/docker.sock dockersamples/visualizer
box "Open web browser to visualize cluster" "light_purple" "green"
open http://$(docker-machine ip leader1):8080

box "To scale type:eval \$(docker-machine env leader1) && docker service scale stack_dbcluster=10 || docker service scale stack_wordpress=10" "red" "red"
